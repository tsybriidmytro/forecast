<?php

namespace ForecastBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SportActionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date')
            ->add('round')
            ->add('homeScore')
            ->add('guestScore')
            ->add('homeValuable')
            ->add('league')
            ->add('homeTeam')
            ->add('guestTeam')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ForecastBundle\Entity\SportAction'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'forecastbundle_sportaction';
    }
}
