<?php

namespace ForecastBundle;

interface IParseStrategy
{
    public function parse($url);

    public function getDataFromHtml($html);

    public function getData($url);

}