<?php

namespace ForecastBundle;

use Symfony\Component\Config\Definition\Exception\Exception;

class ParseService
{
    private $strategy;

    public function parse($url)
    {
        if (isset($this->strategy))
            return $this->strategy->parse($url);
        else
            throw new Exception("Parsing strategy is not defined");

    }

    public function setStrategy($sportType)
    {
        switch ($sportType) {
            case "football":
                $this->strategy = new FootballParse();
                break;
            case "basketball":
                $this->strategy = new BasketballParse();
                break;
            default:
                throw new Exception("Unknown parsing strategy");
        }
    }
}