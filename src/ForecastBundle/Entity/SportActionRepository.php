<?php

namespace ForecastBundle\Entity;

use Doctrine\ORM\EntityRepository;
use ForecastBundle\ForecastBundle;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Tests\Fixtures\Entity;


/**
 * SportActionRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SportActionRepository extends EntityRepository
{

    /**
     * Creates sport actions
     *
     * @param array Array with all data parsed from single HTML page. Contains all needed data to create sport actions.
     * @return array All and added records from data.
     */
    public function setAction($actions)
    {
        $em = $this->getEntityManager();
        $addCounter = 0;
        foreach ($actions as $key => $action) {
            $preparedEnteties = $this->prepareEnteties($action);
            $sportAction = $em->getRepository('ForecastBundle:SportAction')->findBy(array(
                'homeTeam' => $preparedEnteties['homeTeam'],
                'guestTeam' => $preparedEnteties['guestTeam'],
                'date' => new \DateTime($action['date'])));

            if (!$sportAction) {
                $sportAction = new SportAction();
                $sportAction->setHomeTeam($preparedEnteties['homeTeam']);
                $sportAction->setGuestTeam($preparedEnteties['guestTeam']);
                $sportAction->setDate(new \DateTime($action['date']));
                $sportAction->setHomeScore($action['homeScore']);
                $sportAction->setGuestScore($action['guestScore']);
                $sportAction->setHomeValuable(true);
                $sportAction->setRound(1);
                $sportAction->setLeague($preparedEnteties['league']);
                $em->persist($sportAction);
                $addCounter++;
            }
        }
        $em->flush();
        return array('all' => count($actions), 'added' => $addCounter);
    }


    /**
     * Check if all entities are ready to create sport action
     * (leagues, countries, team have been met before and already created)
     *
     * @param array Incoming data to check
     * @return array array of prepared entities
     */
    public function prepareEnteties($action)
    {
        $em = $this->getEntityManager();

        if (!isset($action['homeTeam'], $action['guestTeam'], $action['homeScore'], $action['guestScore'], $action['date'], $action['league']))
            throw new Exception("Not enough parameters to create sport action");

        $result['league'] = $em->getRepository('ForecastBundle:League')->findOneBy(array('name' => $action['league']));
        if (!$result['league']) {
            $result['country'] = $em->getRepository('ForecastBundle:Country')->findOneBy(array('name' => $action['country']));
            if (!$result['country'])
                $action['country'] = $result['country'] = $em->getRepository('ForecastBundle:Country')->createCountry($action);
            else
                $action['country'] = $result['country'];
            $result['league'] = $em->getRepository('ForecastBundle:League')->createLeague($action);
        }
        $result['homeTeam'] = $em->getRepository('ForecastBundle:Team')->findOneBy(array('name' => $action['homeTeam']));
        if (!$result['homeTeam']) {
            $result['homeTeam'] = $em->getRepository('ForecastBundle:Team')->createTeam($action['homeTeam']);
        }
        $result['guestTeam'] = $em->getRepository('ForecastBundle:Team')->findOneBy(array('name' => $action['guestTeam']));
        if (!$result['guestTeam']) {
            $result['guestTeam'] = $em->getRepository('ForecastBundle:Team')->createTeam($action['guestTeam']);
        }

        return $result;
    }
}
