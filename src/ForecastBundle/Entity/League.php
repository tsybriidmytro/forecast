<?php

namespace ForecastBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * League
 */
class League
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $country;

    /**
     * @var integer
     */
    private $sportType;

    /**
     * @var string
     */
    private $year;

    /**
     * @return string
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param string $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * @return int
     */
    public function getSportType()
    {
        return $this->sportType;
    }

    /**
     * @param int $sportType
     */
    public function setSportType($sportType)
    {
        $this->sportType = $sportType;
    }
    /**
     * @return int
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param int $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return League
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
}
