<?php

namespace ForecastBundle\Entity;

use Doctrine\ORM\EntityRepository;
use ForecastBundle\ForecastBundle;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Validator\Tests\Fixtures\Entity;


/**
 * TeamRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class TeamRepository extends EntityRepository
{
    public function createTeam($teamName)
    {
        $em = $this->getEntityManager();
        $team = new Team();
        $team->setName($teamName);
        $em->persist($team);
        $em->flush();
    }
}
