<?php

namespace ForecastBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SportAction
 */
class SportAction
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var integer
     */
    private $round;

    /**
     * @var integer
     */
    private $homeScore;

    /**
     * @var integer
     */
    private $guestScore;

    /**
     * @var boolean
     */
    private $homeValuable;

    /**
     * @var int
     */
    private $league;

    /**
     * @var int
     */
    private $homeTeam;

    /**
     * @return int
     */
    public function getHomeTeam()
    {
        return $this->homeTeam;
    }

    /**
     * @param int $homeTeam
     */
    public function setHomeTeam($homeTeam)
    {
        $this->homeTeam = $homeTeam;
    }

    /**
     * @return int
     */
    public function getGuestTeam()
    {
        return $this->guestTeam;
    }

    /**
     * @param int $guestTeam
     */
    public function setGuestTeam($guestTeam)
    {
        $this->guestTeam = $guestTeam;
    }
    /**
     * @var int
     */
    private $guestTeam;
    /**
     * @return int
     */
    public function getLeague()
    {
        return $this->league;
    }

    /**
     * @param int $league
     */
    public function setLeague($league)
    {
        $this->league = $league;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return SportAction
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set round
     *
     * @param integer $round
     * @return SportAction
     */
    public function setRound($round)
    {
        $this->round = $round;

        return $this;
    }

    /**
     * Get round
     *
     * @return integer 
     */
    public function getRound()
    {
        return $this->round;
    }

    /**
     * Set homeScore
     *
     * @param integer $homeScore
     * @return SportAction
     */
    public function setHomeScore($homeScore)
    {
        $this->homeScore = $homeScore;

        return $this;
    }

    /**
     * Get homeScore
     *
     * @return integer 
     */
    public function getHomeScore()
    {
        return $this->homeScore;
    }

    /**
     * Set guestScore
     *
     * @param integer $guestScore
     * @return SportAction
     */
    public function setGuestScore($guestScore)
    {
        $this->guestScore = $guestScore;

        return $this;
    }

    /**
     * Get guestScore
     *
     * @return integer 
     */
    public function getGuestScore()
    {
        return $this->guestScore;
    }

    /**
     * Set homeValuable
     *
     * @param boolean $homeValuable
     * @return SportAction
     */
    public function setHomeValuable($homeValuable)
    {
        $this->homeValuable = $homeValuable;

        return $this;
    }

    /**
     * Get homeValuable
     *
     * @return boolean 
     */
    public function getHomeValuable()
    {
        return $this->homeValuable;
    }
}
