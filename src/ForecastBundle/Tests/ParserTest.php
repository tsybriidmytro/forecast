<?php

namespace ForecastBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use ForecastBundle;

class ParserTest extends WebTestCase
{
    static $container;
    static $parser;

    /**
     * @dataProvider dataForTest
     */
    public function testFootbalParser($url, $filename)
    {
        $kernel = static::createKernel();
        $kernel->boot();
        self::$container = $kernel->getContainer();
        self::$parser = self::$container->get('parser');
        self::$parser->setStrategy("football");

        $result = self::$parser->parse($url);

        $perfectParse = json_decode(file_get_contents($filename));

        // just compare results of current parse with previous proper results from the file
        foreach ($result as $key => $r) {
            $this->assertEquals($perfectParse[$key]->league, $result[$key]['league']);
            $this->assertEquals($perfectParse[$key]->homeTeam, $result[$key]['homeTeam']);
            $this->assertEquals($perfectParse[$key]->guestTeam, $result[$key]['guestTeam']);
            $this->assertEquals($perfectParse[$key]->country, $result[$key]['country']);
            $this->assertEquals($perfectParse[$key]->homeScore, $result[$key]['homeScore']);
            $this->assertEquals($perfectParse[$key]->guestScore, $result[$key]['guestScore']);
            $this->assertEquals($perfectParse[$key]->sportType, $result[$key]['sportType']);
        }

    }

    public function dataForTest()
    {
        return array(
            array("http://www.sports.ru/borussia/calendar/?&s=3302", "perfectParse.txt"),
        );
    }
}

?>