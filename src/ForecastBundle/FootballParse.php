<?php
namespace ForecastBundle;

use Symfony\Component\CssSelector\CssSelector;
use Symfony\Component\DomCrawler\Crawler;

class FootballParse implements IParseStrategy
{
    public function parse($url)
    {
        $html = $this->getData($url);
        $data = $this->getDataFromHtml($html);
        return $data;
    }

    public function getData($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $html = curl_exec($ch);
        curl_close($ch);
        return $html;
    }

    public function getDataFromHtml($html)
    {
        $data = array();
        $crawler = new Crawler();
        $crawler->add($html);
        $targetTeam = substr($crawler->filter('.titleH1')->html(), 2, strpos($crawler->filter('.titleH1')->html(), "<span") - 4);
        $contentTable = $crawler->filter('.stat-table tbody');
        $contentTable->filter('tr')->each(function (Crawler $tr, $i) use (&$data, $targetTeam) {
            $dateTd = $tr->filter('td')->first()->text();
            $data[$i]['date'] = substr($dateTd, 0, strpos($dateTd, "|"));

            $data[$i]['league'] = $tr->filter('td')->eq(1)->filter('a')->text();
            $countryName = explode(".", $data[$i]['league']);
            $data[$i]['country'] = count($countryName) > 1 ? $countryName[0] : "International";
            $oponentTeam = $tr->filter('td')->eq(2)->text();
            $home = $tr->filter('td')->eq(3)->text() == "Дома" ? 1 : 0;
            $oponentKey = $home == 1 ? "guestTeam" : "homeTeam";
            $selfKey = $home == 0 ? "guestTeam" : "homeTeam";
            $data[$i][$oponentKey] = $oponentTeam;
            $data[$i][$selfKey] = $targetTeam;

            $score = explode(":", $tr->filter('td')->eq(4)->text());
            $data[$i]['homeScore'] = count($score) > 1 ? trim($score[0]) : 0;
            $data[$i]['guestScore'] = count($score) > 1 ? trim($score[1]) : 0;

            $data[$i]['sportType'] = 1;
        });
        return $data;
    }
}