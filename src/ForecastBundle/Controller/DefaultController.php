<?php

namespace ForecastBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('ForecastBundle:Default:index.html.twig', array('name' => $name));
    }

}
