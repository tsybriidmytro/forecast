<?php

namespace ForecastBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use ForecastBundle\Entity\SportAction;
use ForecastBundle\Form\SportActionType;

/**
 * SportAction controller.
 *
 */
class SportActionController extends Controller
{

    /**
     * Lists all SportAction entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ForecastBundle:SportAction')->findAll();

        return $this->render('ForecastBundle:SportAction:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new SportAction entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new SportAction();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('sportaction_show', array('id' => $entity->getId())));
        }

        return $this->render('ForecastBundle:SportAction:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a SportAction entity.
     *
     * @param SportAction $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(SportAction $entity)
    {
        $form = $this->createForm(new SportActionType(), $entity, array(
            'action' => $this->generateUrl('sportaction_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new SportAction entity.
     *
     */
    public function newAction()
    {
        $entity = new SportAction();
        $form   = $this->createCreateForm($entity);

        return $this->render('ForecastBundle:SportAction:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a SportAction entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ForecastBundle:SportAction')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SportAction entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ForecastBundle:SportAction:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing SportAction entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ForecastBundle:SportAction')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SportAction entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ForecastBundle:SportAction:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a SportAction entity.
    *
    * @param SportAction $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(SportAction $entity)
    {
        $form = $this->createForm(new SportActionType(), $entity, array(
            'action' => $this->generateUrl('sportaction_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing SportAction entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ForecastBundle:SportAction')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SportAction entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('sportaction_edit', array('id' => $id)));
        }

        return $this->render('ForecastBundle:SportAction:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a SportAction entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ForecastBundle:SportAction')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find SportAction entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('sportaction'));
    }

    /**
     * Creates a form to delete a SportAction entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('sportaction_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
