<?php

namespace ForecastBundle\Controller;

//use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use ForecastBundle\Entity\Country;
use ForecastBundle\Form\CountryType;
use ForecastBundle\ParseService;


/**
 * Parse controller.
 *
 */
class ParseController extends Controller
{
    public function indexAction()
    {
        $parser = $this->get('parser');
        $parser->setStrategy("football");
        $result = $parser->parse("http://www.sports.ru/juventus/calendar/");

        $res = $this->getDoctrine()->getRepository('ForecastBundle:SportAction')->setAction($result);

        return new Response($res['added'] . " of " . $res['all'] . " sport actions were added");
    }

}
